'use strict';

var app = angular.module('calculator', ['LocalStorageModule', 'ngRoute'])

app.controller('mycontroller', function (localStorageService) {
    this.OPERATORS = ['+', '-', '*', '/'];
    this.NUMBERS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    this.logs = '';

    this.display = 0;
    this.left = null;
    this.right = null;
    this.previous_c = null;
    this.operator = null;
    this.bCalculated = false;

    this.calculate = function () {
        if (this.left == null) return;
        if (this.operator == null) return;
        if (this.right == null) return;

        switch (this.operator) {
            case '+':
                this.display = this.left + this.right;
                break;
            case '-':
                this.display = this.left - this.right;
                break;
            case '*':
                this.display = this.left * this.right;
                break;
            case '/':
                this.display = this.left / this.right;
                break;
        }

        this.left = this.display;
        this.bCalculated = true;
        this.show();
        this.saveState();
    };

    this.show = function () {
        // this.logs = 'left: ' + this.left + '\nright: ' + this.right + '\noperator: ' + this.operator + '\ndisplay: ' + this.display;
    };

    //click event
    this.operatorOnClicked = function (operator) {
        if (!this.bCalculated && this.operator != null && this.left != null && this.right != null) {
            this.calculate();
        }

        this.operator = operator;
        this.left = this.display;
        this.right = null;
        this.previous_c = operator;
        this.show();
        this.saveState();
    };

    this.equalOnClicked = function () {
        this.calculate();
        this.previous_c = '=';
        this.show();
        this.saveState();
    };

    this.numberOnClicked = function (number) {
        if (this.bCalculated && this.previous_c == '=') {
            this.clearOnClicked();
        }

        if (this.OPERATORS.indexOf(this.previous_c) != -1) {
            this.right = number;
        }
        else
            this.right = this.right * 10 + number;
        this.display = this.right;
        this.bCalculated = false;
        this.previous_c = number;
        this.show();
        this.saveState();
    };

    this.clearOnClicked = function () {
        this.display = 0;
        this.left = null;
        this.right = null;
        this.operator = null;
        this.previous_c = null;
        this.bCalculated = false;
        this.show();
        this.saveState();
    };

    this.pos_negOnClicked = function () {
        this.right = this.right * (-1);
        this.display = this.right;
        this.previous_c = '+/-';
        this.saveState();
    };

    this.dotOnClicked = function () {
        this.previous_c = '.';
        this.saveState();
    };

    this.percentageOnClicked = function () {
        this.right = this.right / 100;
        this.display = this.right;
        this.previous_c = '%';
        this.saveState();
    };

    this.saveState = function () {
        localStorageService.set('display', this.display);
        localStorageService.set('left', this.left);
        localStorageService.set('right', this.right);
        localStorageService.set('operator', this.operator);
        localStorageService.set('bCalculated', this.bCalculated);
        localStorageService.set('previous_c', this.previous_c);
    };

    this.restore = function () {
        this.display = localStorageService.get('display') == null ? 0 : localStorageService.get('display');
        this.left = localStorageService.get('left');
        this.right = localStorageService.get('right');
        this.operator = localStorageService.get('operator');
        this.bCalculated = localStorageService.get('bCalculated') == null ? false : localStorageService.get('bCalculated');
        this.previous_c = localStorageService.get('previous_c');
    };

    this.restore();
});